public class ConnectionManager {

    Connection connection;

    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public void makConnection(){
        connection.connect();
    }
}
