import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Connection connection = null;

        ConnectionManager connectionManager = new ConnectionManager();


        Scanner sc = new Scanner(System.in);
        System.out.println("Choose an option  - 1 / 2 / 3");
        int input = sc.nextInt();

        switch(input){

            case 1 : connection = new Connect2Jdbc();
            break;

            case 2 : connection = new Connect2Mongo();
            break;

            case 3 : connection = new Connect2Cassandra();
            break;

        }

        connectionManager.setConnection(connection);
        connectionManager.makConnection();
    }
}
